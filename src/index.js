const fs = require('fs');
const https = require('https');

const io = require('socket.io-client');
const express = require('express');
const bodyParser = require('body-parser');
const config = require ('../config/config.js');
const sensorDataRoutes = require('./routes/sensorData.routes.js');

const url = config.api_url

// Socket
const socket = io(url + '/live_data');
socket.on('connect', function() {
  socket.emit('connect_user', {data: 'User connected!'});
});
socket.on('connect', (data) => {
  console.log(data);
});

//Server Configuration
const port = config.port_server;
const app = express();

app.use(express.static(__dirname));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/sensorData', sensorDataRoutes);

app.set('view engine', 'pug');
app.set('views', './src/templates');


app.set('port', port || 3001);

const optionsHTTPS = {
  key: fs.readFileSync(config.serverKey),
  cert: fs.readFileSync(config.serverCert)
};

https.createServer(optionsHTTPS, app).listen(app.get('port'), () => {
  console.log(`Server: App listening on port ${app.get('port')}! Go to https://localhost:${app.get('port')}/`)
});


// const server = app.listen(port, () => {
//   console.log(`Server Client: Server app listening on port ${port}!` );
// });
