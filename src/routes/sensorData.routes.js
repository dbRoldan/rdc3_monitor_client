const express =require('express')
const axios = require('axios');
const config = require ('../../config/config.js');
const R = require('ramda');


const url = config.api_url
const router = express.Router();
let configAxios = {
  withCredentials: true,
};

router.get("/", (req, res, next) => {
    res.render("index");
});

router.post('/login', async (req, res, next) => {
  if (req.body){
    try {
      let userResult = await axios.post(url + '/login', {
        name: req.body.name,
        password: req.body.password
      }, configAxios);
      let data = userResult.data
      await axios.interceptors.request.use((configAxios) => {
      let token = userResult.headers['set-cookie'][0].replace('token=','');
      token = token.replace('; Path=/','');
      if (token) {
          configAxios.headers.Authorization = token;
      }
        return configAxios;
      });

      let userVerification = data.code
      if (!userVerification){
        res.render("profile", data)
      }
      else{
        res.render("index", data);
      }
    } catch(err) {
      console.error('[ERROR: post /login] - ', err);
      next(err);
    }
  }
  else {
    res.render("index")
  }
});

router.post('/signin', async (req, res) => {
  if (req.body){
    try{
      let userResult = await axios.post(url + '/sign_in', req.body);
      res.render("index", userResult.data);
    }
    catch{
      console.error('[ERROR: post /signin] - ', err);
      next(err);
    }
  }
});

router.post('/update/:id', async (req, res) => {
  if(req.body){
    try{
      let id = req.params.id;
      let userResult = await axios.put(url + '/update/' + id, req.body);
      res.render('profile', userResult.data);
    }catch{
      console.error('[ERROR: put /updateUser] - ', err);
      next(err);
    }
  }
});

router.get('/statistics/:sensor', async(req, res) => {
  let sensor = req.params.sensor
  try {
      let userResult = await axios.get(url + '/statistics/' + sensor, configAxios);
      let allData = userResult.data;
      let lastData = -10;
      //LinearData
      let linearData=[];
      let arr_info = R.zip(allData.normal_distribution.x.splice(lastData), allData.normal_distribution.y.splice(lastData));
      for (let i = 0; i < (-1*lastData); i++) {
        let info = {
          'x': arr_info[i][0],
          'y': arr_info[i][1]
        }
        linearData.push(info);
      }
      // scatterChart
      y_data = allData.dispersion_data.y.splice(lastData)
      let scatterData = [];
      for (let i = 0; i < (-1*lastData); i++){
        let info = {
          'x': i,
          'y': y_data[i]
        }
        scatterData.push(info)
      }

      let data ={
              'sensor_name': allData.sensor_name,
              'average': allData.average,
              'trend': allData.trend,
              'median': allData.median,
              'range':allData.range,
              'variance': allData.variance,
              'standar_deviation': allData.standar_deviation,
              'correlations': allData.correlations,
              'deciles_data':allData.deciles_data,
              'average_per_day': allData.average_per_day,
              'dataLinearGraph': linearData,
              'dataScatterGraph': scatterData
              };
              //console.log(data.dataLinearGraph);
      res.render('statistics', data);
  } catch (err) {
    console.error('[ERROR: get /statistics] - ', err);
    next(err);
  }
});

module.exports = router;
